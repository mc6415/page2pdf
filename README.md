#DOM2PDF

This is a very simple plugin that's still being worked on that allows you to export a DOM element as a PDF.
This could be used for example to export the body element and export the page as a PDF.

There is one method currently that is

```javascript
dom2pdf.downloadPDF(domSelectorQuery, filename)
```

This will take the selected dom element and export it to the filename.pdf 
