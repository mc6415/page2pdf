var html2canvas = require('html2canvas');
var jsPDF = require('jspdf');

const dom2pdf = (function() {
    return {
        downloadPDF: function(domSelector, fileName) {
            html2canvas(document.querySelector(domSelector)).then(canvas => {
                const imgData = canvas.toDataURL('image/png');

                const pdf = new jsPDF();
                pdf.addImage(imgData, 'PNG', 0, 0);
                pdf.save(fileName)
            })
        }
    }
})();

module.exports = dom2pdf;
